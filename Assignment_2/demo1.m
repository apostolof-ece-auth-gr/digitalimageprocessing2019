%% Initialization
clear
clear all

% For reproducibility
rng(1);

load('dip_hw_2.mat');

%% Executes experiments for the affinity matrix
% Clustering into two clusters
clusters = mySpectralClustering(d1a, 2);

% Presents results
clusters = clusters ./ 2;
figure();
imshow(clusters');

% Clustering into three clusters
clusters = mySpectralClustering(d1a, 3);

% Presents results
clusters = clusters ./ 3;
figure();
imshow(clusters');

% Clustering into four clusters
clusters = mySpectralClustering(d1a, 4);

% Presents results
clusters = clusters ./ 4;
figure();
imshow(clusters');
