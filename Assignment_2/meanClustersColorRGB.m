function coloredIm = meanClustersColorRGB(image, clusters)
%meanClustersColorRGB assigns the mean color of each cluster to the cluster
%   Usage coloredIm = meanClustersColorRGB (image, clusters), where:
%       Inputs
%       - image is the original image
%       - clusters is a matrix of the same dimensions as the image that
%           holds the cluster ID of each pixel
%       Output
%       - coloredIm is the image produced by assigning the mean color of
%           each cluster to the cluster's pixels

    % Initializes helper variables
    redChannel = image(:, :, 1);
    greenChannel = image(:, :, 2);
    blueChannel = image(:, :, 3);

    coloredImR = clusters;
    coloredImG = clusters;
    coloredImB = clusters;

    % Assigns the mean color to each cluster
    for cluster = (unique(clusters(:))')
        meanR = mean(redChannel(clusters == cluster));
        meanG = mean(greenChannel(clusters == cluster));
        meanB = mean(blueChannel(clusters == cluster));

        coloredImR(clusters == cluster) = meanR;
        coloredImG(clusters == cluster) = meanG;
        coloredImB(clusters == cluster) = meanB;
    end

    coloredIm = zeros(size(image, 1), size(image, 2), 3);
    coloredIm(:, :, 1) = coloredImR;
    coloredIm(:, :, 2) = coloredImG;
    coloredIm(:, :, 3) = coloredImB;
end

