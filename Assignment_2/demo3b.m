%% Initialization
clear
clear all

% For reproducibility
rng(1);

load('dip_hw_2.mat');

%% Produces the affinity graphs for both images
graph1 = Image2Graph(d2a);
graph2 = Image2Graph(d2b);

%% Executes recursive experiments for the first image
figure();
imshow(d2a);

clusters = myNCuts(graph1, 2);
NCut1 = calculateNcut(graph1, clusters);

% Presents results
clusters = clusters - 1;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);

%% Executes recursive experiments for the second image
figure();
imshow(d2b);

clusters = myNCuts(graph2, 2);
NCut2 = calculateNcut(graph2, clusters);

% Presents results
figure();
imshow(meanClustersColorRGB(d2b, reshape(clusters, size(d2b, 1), [])));

clusters = clusters - 1;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);
