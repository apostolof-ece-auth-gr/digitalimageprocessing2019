# Second deliverable

In the second deliverable we were tasked to implement a number of functions that:
- produce a graph representation of an image
- do image segmentation using spectral clustering
- do image segmentation using the normalized cuts (N-cuts) method, in both an imperative and recursive way
- execute a series of experiments to test the results of our code