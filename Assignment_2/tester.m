image = imageT;
k = 4;

graph = Image2Graph(image);
%clusters = mySpectralClustering(graph, k);
%clusters = myNCuts(graph, k);
clusters = recursiveNCuts(graph);

%calculateNcut(graph, clusters);
unique(clusters)

clusters = reshape(clusters, size(image, 1), []);
imshow(meanClustersColorRGB(imageT, clusters))

clearvars graph clusters k image