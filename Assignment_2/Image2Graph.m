function myAffinityMat = Image2Graph (imIn)
%Image2Graph produces a graph, in the form of a matrix, from an image
%   Usage myAffinityMat = Image2Graph (imIn), where:
%       Inputs
%       - imIn is the input image
%       Output
%       - myAffinityMat is the graph produced

    % Initializes helper variables
    imageWidth = size(imIn, 2);
    imageHeight = size(imIn, 1);
    imageChannels = size(imIn, 3);
    singleLineTransform = reshape(imIn, 1, [], imageChannels);
    
    % Calculates intensity difference per channel per point pair
    myAffinityMat = ...
        repmat(singleLineTransform, imageWidth * imageHeight, 1, 1) - ...
        permute(singleLineTransform, [2 1 3]);
    % Calculates "distance" between each point pair
    myAffinityMat = sum(myAffinityMat .^ 2, 3) .^ 0.5;
    % Calculates affinity between each point pair
    myAffinityMat = 1 ./ exp(myAffinityMat ./ var(imIn(:)));
end
