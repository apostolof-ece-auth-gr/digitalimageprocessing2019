function nCutValue = calculateNcut (anAffinityMat , clusterIdx)
%Calculation of the NCut metric
%   Usage nCutValue = calculateNcut (anAffinityMat , clusterIdx), where:
%       Inputs
%       - anAffinityMat is a rectangular, symmetrical affinity matrix
%           representation of an image
%       - clusterIdx is a vector storing the cluster Id of each node
%       Output
%       - nCutValue is a vector storing the cluster Id of each node

    % Gets the unique cluster IDs
    clusterIds = unique(clusterIdx);
    if size(clusterIds, 1) ~= 2
        error('Too many different clusters! Number of clusters must be two');
    end

    % Finds the indices of the samples of each cluster
    clusterOneIndices = (clusterIdx == clusterIds(1));
    clusterTwoIndices = (clusterIdx == clusterIds(2));

    % Calculates the N-Cut metric
    nCutValue = 2 - ...
        (sum(sum(anAffinityMat(clusterOneIndices, clusterOneIndices))) / ...
        sum(sum(anAffinityMat(clusterOneIndices, :))) + ...
        sum(sum(anAffinityMat(clusterTwoIndices, clusterTwoIndices))) / ...
        sum(sum(anAffinityMat(clusterTwoIndices, :))));
end
