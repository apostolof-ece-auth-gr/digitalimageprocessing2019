%% Initialization
clear
clear all

% For reproducibility
rng(1);

load('dip_hw_2.mat');

%% Produces the affinity graphs for both images
graph1 = Image2Graph(d2a);
graph2 = Image2Graph(d2b);

%% Executes recursive experiments for the first image
figure();
imshow(d2a);

clusters = recursiveNCuts(graph1);

% Presents results
clusters = reshape(clusters, size(d2a, 1), []);
clustersR = clusters;
clustersG = clusters;
clustersB = clusters;

for cluster = 1:size(unique(clusters), 1)
    clustersR(clusters == cluster) = rand;
    clustersG(clusters == cluster) = rand;
    clustersB(clusters == cluster) = rand;
end
clusters(:, :, 1) = clustersR;
clusters(:, :, 2) = clustersG;
clusters(:, :, 3) = clustersB;

figure();
imshow(clusters);

clearvars clustersR clustersG clustersB cluster

%% Executes recursive experiments for the second image
figure();
imshow(d2b);

clusters = recursiveNCuts(graph2);

% Presents results
clusters = reshape(clusters, size(d2b, 1), []);
figure();
imshow(meanClustersColorRGB(d2b, clusters));

clustersR = clusters;
clustersG = clusters;
clustersB = clusters;

for cluster = 1:size(unique(clusters), 1)
    clustersR(clusters == cluster) = rand;
    clustersG(clusters == cluster) = rand;
    clustersB(clusters == cluster) = rand;
end
clusters(:, :, 1) = clustersR;
clusters(:, :, 2) = clustersG;
clusters(:, :, 3) = clustersB;

figure();
imshow(clusters);

clearvars clustersR clustersG clustersB cluster