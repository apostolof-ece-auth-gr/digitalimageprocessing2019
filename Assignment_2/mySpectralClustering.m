function clusterIdx = mySpectralClustering (anAffinityMat, k)
%Implementation of spectral clustering
%   Usage clusterIdx = mySpectralClustering (anAffinityMat, k), where:
%       Inputs
%       - anAffinityMat is a rectangular, symmetrical affinity matrix
%           representation of an image
%       - k is the desired number of clusters
%       Output
%       - clusterIdx is a vector storing the cluster Id of each node

    % Makes sure preconditions are met
    if ~issymmetric(anAffinityMat)
        error('The affinity matrix provided is not symmetric.');
    end
    if k < 2
        error('The number of clusters must be greater than two.');
    end

    % Calculates the eigenvectors
    [eigenvectorsMatrix, ~] = eigs(double( ...
        diag(sum(anAffinityMat, 2)) - anAffinityMat), k, 'sm');
    
    % Does the clustering using K-Means
    clusterIdx = kmeans(eigenvectorsMatrix, k);
end
