%% Initialization
clear
clear all

% For reproducibility
rng(1);

load('dip_hw_2.mat');

%% Produces the affinity graphs for both images
graph1 = Image2Graph(d2a);
graph2 = Image2Graph(d2b);

%% Executes non recursive experiments for the first image
figure();
imshow(d2a);

% Clustering into two clusters
clusters = myNCuts(graph1, 2);

% Presents results
clusters = clusters ./ 2;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);

% Clustering into three clusters
clusters = myNCuts(graph1, 3);

% Presents results
clusters = clusters ./ 3;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);

% Clustering into four clusters
clusters = myNCuts(graph1, 4);

% Presents results
clusters = clusters ./ 4;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);

%% Executes non recursive experiments for the second image
figure();
imshow(d2b);

% Clustering into two clusters
clusters = myNCuts(graph2, 2);

% Presents results
figure();
imshow(meanClustersColorRGB(d2b, reshape(clusters, size(d2b, 1), [])));
clusters = clusters ./ 2;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);

% Clustering into three clusters
clusters = myNCuts(graph2, 3);

% Presents results
figure();
imshow(meanClustersColorRGB(d2b, reshape(clusters, size(d2b, 1), [])));
clusters = clusters ./ 3;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);

% Clustering into four clusters
clusters = myNCuts(graph2, 4);

% Presents results
figure();
imshow(meanClustersColorRGB(d2b, reshape(clusters, size(d2b, 1), [])));
clusters = clusters ./ 4;
clusters = reshape(clusters, size(d2a, 1), []);
figure();
imshow(clusters);
