function q = imagequant(x, w1, w2, w3)
%imagequant quantizes the pixel values of a tri-chromatic image
%   Usage q = imagequant(x, w1, w2, w3), where:
%       Inputs
%       - x is the input image who's values are going to be quantized
%       - w1 is the quantization step size for the first channel (red)
%       - w2 is the quantization step size for the second channel (green)
%       - w3 is the quantization step size for the third channel (blue)
%
%       Output
%       - q is the image with the quantized values

    q(size(x, 1), size(x, 2), 3) = 0;
    q(:, :, 1) = myquant(x(:, :, 1), w1);
    q(:, :, 2) = myquant(x(:, :, 2), w2);
    q(:, :, 3) = myquant(x(:, :, 3), w3);
end
