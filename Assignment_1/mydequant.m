function x = mydequant(q, w)
%Implementation of a uniform, symmetric dequantizer without a dead zone
%   Usage x = mydequant(q, w), where:
%       Inputs
%       - q is the input that is going to be dequantized, this can be a
%       scalar, a column or row vector or a matrix
%       - w is the quantization step size
%
%       Output
%       - x holds the quantized value(s), depending on the input this may
%       be a scalar, a column or row vector or a matrix

    x = (2 * q .* w + w) / 2;
end
