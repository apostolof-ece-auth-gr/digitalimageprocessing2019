function saveasppm(x, filename, K)
%saveasppm encodes an image as PPM and saves it to the disk
%   Usage saveasppm(x, filename, K), where:
%       Inputs
%       - x is the input image
%       - filename is the path and filename of the file where the resulting
%           PPM image is going to be saved
%       - K is the maximum value of brightness

    file = fopen(filename, 'w');
    
    fwrite(file, 'P6');
    fwrite(file, ' ');
    fwrite(file, int2str(size(x, 2)));
    fwrite(file, ' ');
    fwrite(file, int2str(size(x, 1)));
    fwrite(file, ' ');
    fwrite(file, int2str(K));
    fwrite(file, newline);
    
    if K < 256
        precision = 'integer*1';
        machinefmt = 'l';
    else
        precision = 'integer*2';
        machinefmt = 'b';
    end
    
    for row = 1:size(x, 1)
        combinedRow(1:3:3 * size(x, 2)) = x(row, :, 1);
        combinedRow(2:3:3 * size(x, 2)) = x(row, :, 2);
        combinedRow(3:3:3 * size(x, 2)) = x(row, :, 3);
        fwrite(file, combinedRow, precision, machinefmt);
    end

    fclose(file);
end

