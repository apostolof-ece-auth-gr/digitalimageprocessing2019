# First deliverable

In this first deliverable we were called to implement a number of functions that:
- produce an RBG image from a Bayer pattern array of samples
- quantize and dequantize the image using any desired number of bits
- transform the image to the PPM format and save it to disk