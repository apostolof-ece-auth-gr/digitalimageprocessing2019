function x = imagedequant(q, w1, w2, w3)
%imagedequant dequantizes the pixel values of a tri-chromatic image
%   Usage x = imagedequant(q, w1, w2, w3), where:
%       Inputs
%       - q is the input image who's values are going to be dequantized
%       - w1 is the quantization step size for the first channel (red)
%       - w2 is the quantization step size for the second channel (green)
%       - w3 is the quantization step size for the third channel (blue)
%
%       Output
%       - x is the image with the dequantized values

    x(size(q, 1), size(q, 2), 3) = 0;
    x(:, :, 1) = mydequant(q(:, :, 1), w1);
    x(:, :, 2) = mydequant(q(:, :, 2), w2);
    x(:, :, 3) = mydequant(q(:, :, 3), w3);
end
