function q = myquant(x, w)
%Implementation of a uniform, symmetric quantizer without a dead zone
%   Usage q = myquant(x, w), where:
%       Inputs
%       - x is the input that is going to be quantized, this can be a
%       scalar, a column or row vector or a matrix
%       - w is the quantization step size
%
%       Output
%       - q holds the quantized value(s), depending on the input this may
%       be a scalar, a column or row vector or a matrix

    q = floor(x ./ w);
end
